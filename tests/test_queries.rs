extern crate wikibase;

use wikibase::query::{EntityQuery, SearchQuery};

#[test]
fn test_single_entity_query() {
    let configuration = wikibase::Configuration::new("Automatic-Testing/0.1").unwrap();
    let entity_query = EntityQuery::new(vec!["Q575650".to_string()], "en");

    assert_eq!(vec!["Q575650"], entity_query.ids());
    assert_eq!("en", entity_query.lang());
    assert_eq!("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q575650&languages=en&uselang=en&format=json",
    entity_query.url(&configuration));
}

#[test]
fn test_multiple_entity_query() {
    let configuration = wikibase::Configuration::new("Automatic-Testing/0.1").unwrap();
    let entity_query = EntityQuery::new(vec!["Q575650".to_string(), "P4763".to_string()], "cs");

    assert_eq!(vec!["Q575650", "P4763"], entity_query.ids());
    assert_eq!("cs", entity_query.lang());
    assert_eq!("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q575650|P4763&languages=cs&uselang=cs&format=json",
    entity_query.url(&configuration));
}

#[test]
fn test_search_query() {
    let entity_type = wikibase::EntityType::Item;
    let search_query = SearchQuery::new("linux", "es", "it", 20, entity_type);

    assert_eq!("linux", search_query.query());
    assert_eq!(20u64, *search_query.limit());
    assert_eq!(wikibase::EntityType::Item.string_value(), search_query.entity_type().string_value());
    assert_eq!("es", search_query.lang());
    assert_eq!("it", search_query.search_lang());
}
