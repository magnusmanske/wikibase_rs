extern crate wikibase;

#[test]
fn test_monolingual_text() {
    let mut monolingual_text = wikibase::MonoLingualText::new("Львів", "uk");
    assert_eq!("uk", monolingual_text.language());
    assert_eq!("Львів", monolingual_text.text());

    monolingual_text.set_text("Warszawa");
    monolingual_text.set_language("pl");

    assert_eq!("pl", monolingual_text.language());
    assert_eq!("Warszawa", monolingual_text.text());
}

#[test]
fn test_coordinates() {
    let mut coordinate = wikibase::Coordinate::new(
        Some(100f64),
        "http://www.wikidata.org/entity/Q2".to_string(),
        12f64,
        6f64,
        Some(0f64));
    assert_eq!(100f64, coordinate.altitude().unwrap());
    assert_eq!("http://www.wikidata.org/entity/Q2", coordinate.globe());
    assert_eq!(6f64, *coordinate.longitude());
    assert_eq!(12f64, *coordinate.latitude());
    assert_eq!(0f64, coordinate.precision().unwrap());

    coordinate.set_altitude(Some(200f64));
    coordinate.set_globe("http://www.wikidata.org/entity/Q111");
    coordinate.set_longitude(47.153333333333);
    coordinate.set_latitude(9.8219444444444);
    coordinate.set_precision(Some(10f64));

    assert_eq!(200f64, coordinate.altitude().unwrap());
    assert_eq!("http://www.wikidata.org/entity/Q111", coordinate.globe());
    assert_eq!(47.153333333333, *coordinate.longitude());
    assert_eq!(9.8219444444444, *coordinate.latitude());
    assert_eq!(10f64, coordinate.precision().unwrap());
}

#[test]
fn test_entity_value() {
    let mut item = wikibase::EntityValue::new(
        wikibase::EntityType::new_from_str("item").unwrap(), "Q212730");
    assert_eq!("Q212730", item.id());

    item.set_id("P4539");
    item.set_entity_type(wikibase::EntityType::new_from_str("property").unwrap());
    assert_eq!("P4539", item.id());
}

#[test]
fn test_entity_type() {
    let property = wikibase::EntityType::new_from_id("P17");
    assert_eq!(wikibase::EntityType::Property.string_value(), property.unwrap().string_value());

    let item = wikibase::EntityType::new_from_id("Q9346299");
    assert_eq!(wikibase::EntityType::Item.string_value(), item.unwrap().string_value());
}

#[test]
fn test_quantity_value() {
    let mut quantity = wikibase::QuantityValue::new(3f64, Some(1f64),
        "http://www.wikidata.org/entity/Q11574", Some(5f64));

    assert_eq!(3f64, *quantity.amount());
    assert_eq!(1f64, quantity.lower_bound().unwrap());
    assert_eq!("http://www.wikidata.org/entity/Q11574", quantity.unit());
    assert_eq!(5f64, quantity.upper_bound().unwrap());

    quantity.set_amount(150f64);
    quantity.set_lower_bound(Some(100f64));
    quantity.set_unit("http://www.wikidata.org/entity/Q11573");
    quantity.set_upper_bound(Some(200f64));

    assert_eq!(150f64, *quantity.amount());
    assert_eq!(100f64, quantity.lower_bound().unwrap());
    assert_eq!("http://www.wikidata.org/entity/Q11573", quantity.unit());
    assert_eq!(200f64, quantity.upper_bound().unwrap());
}

#[test]
fn test_time_value() {
    let mut time = wikibase::TimeValue::new(0, 0, "http://www.wikidata.org/entity/Q1985727",
        11, "+2017-10-28T00:00:00Z", 0);

    assert_eq!(0u64, *time.after());
    assert_eq!(0u64, *time.before());
    assert_eq!("http://www.wikidata.org/entity/Q1985727", time.calendarmodel());
    assert_eq!(11u64, *time.precision());
    assert_eq!("+2017-10-28T00:00:00Z", time.time());
    assert_eq!(0u64, *time.timezone());

    time.set_after(1u64);
    time.set_before(2u64);
    time.set_calendarmodel("http://www.wikidata.org/entity/Q11184");
    time.set_precision(22u64);
    time.set_time("+2018-10-28T00:00:00Z");
    time.set_timezone(3u64);

    assert_eq!(1u64, *time.after());
    assert_eq!(2u64, *time.before());
    assert_eq!("http://www.wikidata.org/entity/Q11184", time.calendarmodel());
    assert_eq!(22u64, *time.precision());
    assert_eq!("+2018-10-28T00:00:00Z", time.time());
    assert_eq!(3u64, *time.timezone());
}

#[test]
fn test_data_value_type() {
    let data_value_type = wikibase::DataValueType::new_from_str("quantity");
    assert_eq!("quantity", data_value_type.unwrap().string_value());
}

#[test]
fn test_data_value() {
    let monolingual_text = wikibase::MonoLingualText::new("Helsinki", "fi");
    let value = wikibase::Value::MonoLingual(monolingual_text);
    let data_value_type = wikibase::DataValueType::new_from_str("monolingualtext").unwrap();
    let mut data_value = wikibase::DataValue::new(data_value_type, value);

    assert_eq!("monolingualtext", data_value.value_type().string_value());

    match data_value.value() {
        &wikibase::Value::MonoLingual(ref returned_monolingual) => {
            assert_eq!("Helsinki", returned_monolingual.text());
            assert_eq!("fi", returned_monolingual.language());
        }
        _ => {}
    };

    let new_value = wikibase::Value::StringValue("София".to_string());
    data_value.set_value(new_value);
    let new_data_value_type = wikibase::DataValueType::new_from_str("string").unwrap();
    data_value.set_value_type(new_data_value_type);

    match data_value.value() {
        &wikibase::Value::StringValue(ref string_value) => {
            assert_eq!("София", string_value);
        }
        _ => {}
    };
}
